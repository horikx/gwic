const express = require('express')
const server = express()
const request = require('request')

__dirname = 'C:/Users/chori/Documents/Ivory Files/2018/20180405 - GWIC/cutup/actual/'

server.get('/', function (req, res) {
    res.sendFile(__dirname + 'index.html')
})
server.get('/about', function (req, res) {
    res.sendFile(__dirname + 'about.html')
})
server.get('/home2', function (req, res) {
    res.sendFile(__dirname + 'home2.html')
})
server.get('/home3', function (req, res) {
  res.sendFile(__dirname + 'home3.html')
})
server.get('/home4', function (req, res) {
  res.sendFile(__dirname + 'home4.html')
})
server.get('/form', function (req, res) {
  res.sendFile(__dirname + 'form.html')
})
server.get('/project', function (req, res) {
  res.sendFile(__dirname + 'project.html')
})

server.use('/css', express.static(__dirname + '/css'))
server.use('/linearicons', express.static(__dirname + '/linearicons'))
server.use('/bower_components', express.static(__dirname + '/bower_components'))
server.use('/js', express.static(__dirname + '/js'))
server.use('/images', express.static(__dirname + '/images'))
server.use('/unify', express.static(__dirname + '/unify'))
server.listen(3000)