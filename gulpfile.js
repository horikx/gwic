var sass = require('gulp-sass')(require('node-sass'));
var sourcemaps = require('gulp-sourcemaps');
var prefix = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var {reload} = browserSync;
var nodemon = require('gulp-nodemon');
const { src, series, watch, dest } = require('gulp');

function dosass () {
    return src('styles/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}, {errLogToConsole: true}))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(prefix("last 2 versions"))
        .pipe(sourcemaps.write('.'))
        .pipe(dest('css'))
        .pipe(reload({stream:true}));
};

function browserSyncStart(done) {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    done()
};

// gulp.task('movepearicons', function(){
//     // the base option sets the relative root for the set of files,
//     // preserving the folder structure
//     gulp.src('./pearicons/**/*.*', { base: './' })
//         .pipe(gulp.dest('css'));
// });

function watchFiles (done) {
    watch("scss/**/*", series(sass, reload))
    watch("styles/**/*", series(sass, reload))
    watch("*.html", series(reload))
    done()
}

const dev = series(
        dosass,
        browserSyncStart,
        watchFiles
    )
;

function nodemon (cb) {
    var called = false;
    return nodemon({script: 'scripts/server.js'}).on('start', function () {
        if (!called) {
            called = true;
            cb();
        }
    });
};

exports.sass = dosass
exports.nodemon = nodemon

exports.default = dev